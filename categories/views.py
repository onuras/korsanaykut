from django.core.cache import cache
from .models import Category


def categories_processor(request):
    """ Passes main categories to every request.
        This list is used in navigation. """
    main_categories = cache.get("main_categories")
    if not main_categories:
        main_categories = Category.objects.filter(
                              parent=Category.objects.get(name="Roman"))
        cache.set("main_categories", main_categories)
    return {"main_categories": main_categories}
