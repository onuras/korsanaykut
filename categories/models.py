from django.db import models
from mptt.models import MPTTModel, TreeForeignKey
from autoslug import AutoSlugField


class Category(MPTTModel):
    name = models.CharField(max_length=50)
    slug = AutoSlugField(populate_from='name')
    parent = TreeForeignKey('self', null=True, blank=True,
                            related_name='children', db_index=True)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name_plural = "categories"
