from django.views import generic
from .models import Book


class IndexView(generic.ListView):
    template_name = 'books/list.html'

    def get_queryset(self):
        return Book.objects.order_by('-id')[:5]
