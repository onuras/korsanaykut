from django.core.management.base import BaseCommand
from django.core.exceptions import ObjectDoesNotExist
from django.core.files import File as DjangoFile
from files.models import File
from books.models import Book
from authors.models import Author
from categories.models import Category
from crawlers import idefix
import os
import logging
import hashlib
import traceback


logger = logging.getLogger(__name__)


class Command(BaseCommand):
    help = "Adds books from a directory"

    def add_arguments(self, parser):
        parser.add_argument('--path', type=str, default=os.getcwd())
        parser.add_argument('--extension', type=str, default="epub")

    def handle(self, *args, **options):
        for root, dirs, files in os.walk(options['path']):
            for file in files:
                filename, extension = os.path.splitext(file)
                if extension.endswith(options['extension']):
                    try:
                        add_file(root + "/" + file, filename)
                    except Exception as e:
                        logger.error("Failed to add '%s': %s",
                                     filename, traceback.format_exc())


def add_file(path, filename):
    """ Adds an ebook to database.

    :path: Full path of the file.
    :filename: Name of file without extension.
    :returns: Book object if file successfully added, otherwise None.
    """
    # aykut is using: "{AUTHOR} - {BOOK NAME}" file names for his epubs.
    # filename doesn't contain epub suffix but some books are in reverse order:
    # "{BOOK NAME} - {AUTHOR}". Searching entire string in idefix should
    # return pretty accurate results. But some of the books doesn't exists in
    # idefix and some of them have different authors or publishers (they can be
    # a different book). To eliminate adding unaccurate results we need to
    # check author name, thus same author only written same book once. Then
    # we can compare author name and be sure we are adding correct book details
    # with the file.

    logger.debug("Trying to add file '%s'", path)

    # First we need to check checksum of file to avoid adding duplicated files
    if File.objects.filter(sha256=sha256sum(path)).count() > 0:
        logger.info("{} already found in database. SKIPPING".format(filename))
        return

    # Search filename in idefix
    search_result = search_book(filename)
    if not search_result:
        logger.info("{} not found in idefix. SKIPPING".format(filename))
        return

    # Get book details and save book into database if it's not exist
    logger.debug("Getting book details from '%s'", search_result["url"])
    book_details = idefix.details(search_result["url"])
    try:
        book = Book.objects.get(isbn=book_details["isbn"])
    except ObjectDoesNotExist:
        book = add_book(book_details, search_result['url'])

    # And finally add file into database and book
    with open(path, "rb") as file:
        file = File(file=DjangoFile(file))
        file.save()
        book.files.add(file)

    logger.debug("'%s' successfully added into database", filename)
    return book


def add_book(book_details, url):
    """ Adds new book into database.

    :book_details: Book details from crawlers.idefix.details
    :url: idefix.com url for the book.
    :returns: Book model object.
    """
    book = Book(
        name=book_details['name'],
        description=book_details['description'],
        idefix_url=url,
        isbn=book_details['isbn'],
        category=add_categories(book_details["categories"]),
    )

    cover = None
    if book_details["cover"]:
        cover = idefix.download(book_details["cover"])

    book.save(cover=cover)

    # A book can have more than one author but idefix.com only supports
    # one author which is sucks.
    author = add_author(book_details["author"], book_details["author_url"])
    book.authors.add(author)

    return book


def add_categories(categories):
    """ Adds list of categories into database and returns last category.
        Raises ValueError if categories list is empty.

    :categories: List of categories usually from crawlers.idefix.details.
    :returns: Category model object.
    """
    if type(categories) is not list or len(categories) == 0:
        raise ValueError
    try:
        category = Category.objects.get(name=categories[-1])
    except ObjectDoesNotExist:
        parent = None
        category = None
        for category_name in categories:
            category = Category.objects.get_or_create(name=category_name,
                                                      parent=parent)[0]
            parent = category
    return category


def add_author(author, author_url):
    """ Adds author into database

    :author: Name of author.
    :author_url: idefix.com URL for author.
    :returns: Author model object.
    """
    try:
        author = Author.objects.get(name=author, idefix_url=author_url)
    except ObjectDoesNotExist:
        author = Author(name=author, idefix_url=author_url)
        author.save()
    return author


def sha256sum(path):
    """ Calculates SHA256 sum of a file.

    :path: Path of the file.
    :returns: SHA256 sum of the file.
    """
    hasher = hashlib.sha256()
    with open(path, 'rb') as f:
        hasher.update(f.read())
    return hasher.hexdigest()


def search_book(filename):
    """ Searches filename in idefix and tries to match author name

    :filename: Book filename in aykut's format.
    """
    # Try to get author name and book name from filename
    # Some books are in reverse order (book name comes before author)
    # this needs to considered when checking file integrity
    try:
        (author, name) = filename.split(" - ")[:2]
    except ValueError:
        return None
    for book in idefix.search(filename):
        for author in author.split(","):
            if book['author'].strip().lower() == author.strip().lower():
                return book
        # Assume book name is actually author name and try to match author
        # name with book name
        for author in name.split(","):
            if book['author'].strip().lower() == author.strip().lower():
                return book
    return None
