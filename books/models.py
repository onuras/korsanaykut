from django.db import models
from django.conf import settings
from authors.models import Author
from categories.models import Category
from files.models import File
from autoslug import AutoSlugField
from PIL import Image
import os


_THUMBNAIL_SIZES = [[300, 1000], [100, 1000], [50, 1000]]


class Book(models.Model):
    name = models.CharField(max_length=50)
    slug = AutoSlugField(populate_from='name')
    description = models.TextField()
    idefix_url = models.CharField(max_length=200)
    authors = models.ManyToManyField(Author, blank=True)
    isbn = models.CharField(max_length=50)
    category = models.ForeignKey(Category, on_delete=models.PROTECT)
    cover = models.BooleanField(default=False)
    files = models.ManyToManyField(File, related_name="files")

    def __str__(self):
        return self.name

    def save(self, cover=None, *args, **kwargs):
        self.cover = True if cover is not None or self.cover else False
        super(Book, self).save(*args, **kwargs)

        if cover is not None:
            save_dir = os.path.join(settings.MEDIA_ROOT, 'books/covers/')
            if not os.path.exists(save_dir):
                os.makedirs(save_dir)
            for i in range(-1, len(_THUMBNAIL_SIZES)):
                self._create_thumbnail(cover, i, save_dir)

    def _create_thumbnail(self, cover, size_idx, save_dir):
        img = Image.open(cover)

        # If index is -1 save the original file
        if size_idx == -1:
            img.save(os.path.join(save_dir,
                                  "{}_o.jpg".format(self.id)),
                     "JPEG",
                     quality=95)
        # Save thumbnails if index is higher than -1
        else:
            img.thumbnail(_THUMBNAIL_SIZES[size_idx])
            img.save(os.path.join(save_dir,
                                  "{}_{}.jpg".format(self.id, size_idx)),
                     "JPEG",
                     quality=95)
