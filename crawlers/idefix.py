""" Simple web crawler for idefix.com to search and get book details """

import tempfile
import requests
import os
from bs4 import BeautifulSoup


def search(text):
    """ Searches books in idefix.com.

    :text: Text to search in idefix.com.
    :returns: Generator of book names and urls.
    """
    res = requests.get("http://www.idefix.com/search", params={"q": text})
    soup = BeautifulSoup(res.text, "html.parser")
    for book in soup.find_all(class_="list-cell"):
        summary = book.find(class_="summary")
        name = summary.find("a", class_="item-name")
        author = summary.find("a", class_="who")
        yield {
            "name": name.text,
            "author": author.text,
            "url": "http://www.idefix.com" + name.get("href"),
        }


def details(book_url):
    """ Gets book details from idefix.com book url.

    :book_url: Idefix.com book URL.
    :returns: Book details.
    """
    res = requests.get(book_url)
    soup = BeautifulSoup(res.text, "html.parser")
    name = soup.find("h1").text
    (author, publisher) = soup.find_all(class_="author")[1:3]
    cover = soup.find('a', class_='ImageZoom').get('href')
    description = soup.find(class_="product-description")
    isbn = description.find(attrs={"itemprop": "isbn"}).text
    categories = [c.text for c in soup.find("ul", {"itemprop": "breadcrumb"})
                                      .find_all("a")[2:]]

    return {
        "name": name,
        "author": author.find("a").text,
        "author_url": "http://www.idefix.com" + author.find("a").get('href'),
        "description": description.text,
        "categories": categories,
        "publisher": publisher.find("a").text,
        "cover": cover,
        "url": book_url,
        "isbn": isbn[6:],
    }


def download(url):
    """ Downloads URL and saves it to a temporary file.
        Only text/html, image/jpeg and image/png mimetypes will get a suffix.
        User needs to remove temporary file after he is done.

    :url: File URL.
    :return: Temporary file path.
    """
    response = requests.get(url, stream=True)
    content_type = response.headers.get("Content-Type")
    suffix = ""
    if content_type == "text/html":
        suffix = ".html"
    elif content_type == "image/jpeg":
        suffix = ".jpg"
    elif content_type == "image/png":
        suffix = ".png"
    fd, filename = tempfile.mkstemp(suffix=suffix)
    with os.fdopen(fd, 'wb') as file:
        for buff in response.iter_content(1024):
            file.write(buff)
    return filename
