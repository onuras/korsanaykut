from django.db import models
from autoslug import AutoSlugField


class Author(models.Model):
    name = models.CharField(max_length=50)
    slug = AutoSlugField(populate_from='name')
    description = models.TextField(null=True, blank=True)
    idefix_url = models.CharField(max_length=200)

    def __str__(self):
        return self.name
