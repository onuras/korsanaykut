import unittest
import os
from crawlers import idefix


class TestIdefixCrawler(unittest.TestCase):
    def test_search(self):
        books = list(idefix.search("Kürk Mantolu Madonna"))
        self.assertEqual(len(books), 42)
        self.assertEqual(books[0]["name"], "Kürk Mantolu Madonna")
        self.assertEqual(books[0]["author"], "Sabahattin Ali")
        self.assertEqual(books[0]["url"],
                         "http://www.idefix.com/Kitap/Kurk-Mantolu-Madonna/"
                         "Sabahattin-Ali/Edebiyat/Roman/Turkiye-Roman/"
                         "urunno=0000000058245")

    def test_details(self):
        details = idefix.details("http://www.idefix.com/Kitap/"
                                 "Kurk-Mantolu-Madonna/"
                                 "Sabahattin-Ali/Edebiyat/Roman/Turkiye-Roman/"
                                 "urunno=0000000058245")
        self.assertEqual(details["name"], "Kürk Mantolu Madonna")
        self.assertEqual(details["author"], "Sabahattin Ali")
        self.assertEqual(details["author_url"],
                         "http://www.idefix.com/Yazar/sabahattin-ali/s=253771")
        self.assertEqual(details["publisher"], "Yapı Kredi Yayınları")
        self.assertEqual(details["cover"],
                         "http://i.idefix.com/cache/600x600-0/"
                         "originals/0000000058245-1.jpg")
        self.assertGreater(len(details["description"]), 0)
        self.assertEqual(details["isbn"], "9789753638029")
        self.assertEqual(len(details["categories"]), 3)
        self.assertEqual(details["categories"][0], "Edebiyat")
        self.assertEqual(details["categories"][1], "Roman")
        self.assertEqual(details["categories"][2], "Türkiye Roman")

    def test_download(self):
        filename = idefix.download("http://example.com/")
        self.assertTrue(filename.endswith(".html"))
        os.remove(filename)
