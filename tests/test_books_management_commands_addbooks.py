from django.test import TestCase, override_settings
from django.conf import settings
from books.management.commands import addbooks
from django.core.files import File
from files.models import File as FileModel
from authors.models import Author
from categories.models import Category
from books.models import Book
import os


class TestAddBooksManagementCommand(TestCase):
    def test_sha256sum(self):
        sha256sum = addbooks.sha256sum('tests/test_data/file.txt')
        self.assertEqual(sha256sum, '1a36b5d255585e081caa8011527fb02b'
                                    '64019f2c719ad59e8ee4011c7bdb978d')

    def test_search_book(self):
        book = addbooks.search_book("Sabahattin Ali - Kürk Mantolu Madonna")
        self.assertEqual(book["name"], "Kürk Mantolu Madonna")
        self.assertEqual(book["author"], "Sabahattin Ali")

    def test_avoid_duplicated_files(self):
        # Add test file first
        fp = open("tests/test_data/file.txt", "rb")
        myfile = File(fp)
        f = FileModel(file=myfile)
        f.save()

        # Add file should avoid adding duplicated files and return None
        result = addbooks.add_file("tests/test_data/file.txt", "file")
        self.assertEqual(result, None)

        # Remove test file in database
        os.remove(f.file.path)
        f.delete()

    def test_skip_nonexisted_book(self):
        result = addbooks.add_file("tests/test_data/file.txt",
                                   "A NON EXISTED BOOK NAME WHICH - "
                                   "WONT RETURN ANYTHING IN IDEFIX")
        self.assertEqual(result, None)

    def test_add_categories(self):
        c1 = addbooks.add_categories(["c1", "c2"])
        c2 = addbooks.add_categories(["c1", "c2"])
        self.assertIsInstance(c1, Category)
        self.assertIsInstance(c2, Category)
        self.assertEqual(c1, c2)
        self.assertEqual(c1.name, "c2")
        self.assertEqual(c2.name, "c2")
        self.assertEqual(c1.id, c2.id)
        self.assertEqual(c1.parent.name, "c1")
        self.assertEqual(c2.parent.name, "c1")

    def test_add_categories_exception(self):
        self.assertRaises(ValueError, addbooks.add_categories, [])
        self.assertRaises(ValueError, addbooks.add_categories, "a-str-arg")

    def test_add_author(self):
        author = addbooks.add_author("SOMEONE", "SOMEURL")
        self.assertIsInstance(author, Author)

    @override_settings(MEDIA_ROOT=os.path.join(settings.BASE_DIR,
                                               'media_tmp_add_file'))
    def test_add_file(self):
        book = addbooks.add_file("tests/test_data/file.txt",
                                 "Sabahattin Ali - Kürk Mantolu Madonna")
        self.assertIsInstance(book, Book)
        self.assertEqual(book.name, "Kürk Mantolu Madonna")
        self.assertEqual(book.isbn, "9789753638029")
        self.assertGreater(len(book.description), 0)
        self.assertEqual(book.idefix_url,
                         "http://www.idefix.com/Kitap/Kurk-Mantolu-Madonna/"
                         "Sabahattin-Ali/Edebiyat/Roman/Turkiye-Roman/"
                         "urunno=0000000058245")
        self.assertEqual(book.authors.all()[0].name, "Sabahattin Ali")
        self.assertEqual(book.category.name, "Türkiye Roman")

        book_cover_dir = os.path.join(settings.BASE_DIR,
                                      'media_tmp_add_file/books/covers')

        self.assertTrue(os.path.exists(
            os.path.join(book_cover_dir, "{}_o.jpg".format(book.id))))
        self.assertTrue(os.path.exists(
            os.path.join(book_cover_dir, "{}_0.jpg".format(book.id))))
        self.assertTrue(os.path.exists(
            os.path.join(book_cover_dir, "{}_1.jpg".format(book.id))))
        self.assertTrue(os.path.exists(
            os.path.join(book_cover_dir, "{}_2.jpg".format(book.id))))

        # Test is done here
        # Remove files
        for file in book.files.all():
            os.remove(file.file.path)

        # Remove cover and thumbnails
        os.remove(os.path.join(book_cover_dir, "{}_o.jpg".format(book.id)))
        for i in range(3):
            os.remove(os.path.join(
                book_cover_dir, "{}_{}.jpg".format(book.id, i)))
        os.rmdir(book_cover_dir)
        os.rmdir(os.path.join(settings.BASE_DIR,
                              'media_tmp_add_file/books'))
        os.rmdir(os.path.join(settings.BASE_DIR,
                              'media_tmp_add_file'))
