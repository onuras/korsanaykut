from os import remove
from django.test import TestCase
from django.core.files import File
from files.models import File as FileModel


class TestFileModel(TestCase):
    def test_save(self):
        fp = open("tests/test_data/file.txt", "rb")
        myfile = File(fp)
        f = FileModel(file=myfile)
        f.save()

        self.assertEqual(f.sha256, '1a36b5d255585e081caa8011527fb02b'
                                   '64019f2c719ad59e8ee4011c7bdb978d')
        self.assertEqual(f.size, 17)
        self.assertEqual(f.mime, 'text/plain')
        self.assertTrue(f.file.name.endswith(".txt"))

        # Remove file after we are done
        remove(f.file.path)
        f.delete()
