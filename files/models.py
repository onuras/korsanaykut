from django.db import models
from os.path import splitext
import hashlib
import magic


_THUMBNAIL_SIZES = [[300, 1000], [100, 1000], [50, 1000]]


class File(models.Model):
    mime = models.CharField(max_length=50)
    size = models.IntegerField()
    sha256 = models.CharField(max_length=64, unique=True)
    file = models.FileField()

    def save(self, *args, **kwargs):
        content = self.file.read()
        self.sha256 = hashlib.sha256(content).hexdigest()
        self.size = len(content)
        self.mime = magic.from_buffer(content, mime=True)
        filename, extension = splitext(self.file.name)
        self.file.name = self.sha256
        if extension:
            self.file.name += extension

        super(File, self).save(*args, **kwargs)
